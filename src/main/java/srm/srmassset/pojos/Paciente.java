package srm.srmassset.pojos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Paciente {
	private String nome;
	private Integer idade;
	private SexoEnum sexo;
	private Double altura;
	private Integer peso;

}
