package srm.srmassset.pojos;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import lombok.Getter;

public enum SexoEnum {
	MASCULINO(1),
	FEMININO(0);

	private static final Map<Integer, SexoEnum> LOOKUP = new HashMap();

	static {
		for (SexoEnum e : EnumSet.allOf(SexoEnum.class)) {
			LOOKUP.put(e.getCodigo(), e);
		}
	}

	public static SexoEnum valueByCodigo(Integer codigo) {
		return LOOKUP.get(codigo);
	}

	@Getter
	private Integer codigo;


	SexoEnum(Integer codigo) {
		this.codigo = codigo;
	}

}
