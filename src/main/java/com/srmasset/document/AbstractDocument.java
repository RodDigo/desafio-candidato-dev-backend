package com.srmasset.document;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.annotation.Id;

@Getter
@Setter
public abstract class AbstractDocument {

	@Id
	protected String id;

}
