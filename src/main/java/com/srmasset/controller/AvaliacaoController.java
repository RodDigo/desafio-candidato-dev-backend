package com.srmasset.controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import com.github.javafaker.Faker;

import srm.srmassset.pojos.Paciente;
import srm.srmassset.pojos.SexoEnum;

// 1 - quantidade de pacientes.
// 2 - media de idade dos homens
// 3 - qtd de mulheres com altura entre 1,60 e 170 e peso acima de 70kg
// 4 - qtd de pessoas  com idade entre 18 e 25
// 5 - o nome dos pacientes mais vlehos
// 6 - o nome da mulher mais baixa
// 7 - construir uma situação interessante e construir um algoritmo.

public abstract class AvaliacaoController {

	public static final Locale BRAZIL = new Locale("pt", "BR");
	public static final Faker faker = new Faker(BRAZIL);

	public static List<Paciente> criaListaPaciente() {
		final List<Paciente> listPaciente = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			listPaciente.add(toBuild());
		}
		return listPaciente;
	}

	// OK
	public static List<String> nomePacientesMaisVelhos(List<Paciente> list) {
		final List<Paciente> listPacientes = list.stream()
				.filter(p -> p.getSexo() == SexoEnum.MASCULINO)
				.sorted(Comparator.comparing(Paciente::getIdade).reversed())
				.limit(5)
				.collect(Collectors.toList());

		final List<String> listNomes = listPacientes.parallelStream().map(p -> p.getNome()).collect(Collectors.toList());

		return listNomes;
	}

	// OK
	public static Double mediaIdadePacientesHomens(List<Paciente> list) {
		final Double media = list.stream()
				.filter(p -> p.getSexo() == SexoEnum.MASCULINO)
				.mapToDouble(p -> p.getIdade()).average().orElse(0.0);
		return media;
	}

	// OK
	public static String nomeMulherMaisBaixa(List<Paciente> list) {
		final Paciente paciente = list.stream()
				.filter(p -> p.getSexo() == SexoEnum.FEMININO)
				.min(Comparator.comparing(Paciente::getAltura)).get();
		return paciente.getNome();
	}

	// OK
	public static Integer qtdMulheresEntreAlturasAcimaPeso(List<Paciente> list) {
		final List<Paciente> listPacientes =  list.stream()
				.filter(p -> p.getSexo() == SexoEnum.FEMININO)
				.filter(p -> p.getAltura() > 1.60 && p.getAltura() < 1.70)
				.filter(p -> p.getPeso() > 70)
				.collect(Collectors.toList());
		return listPacientes.size();
	}

	// OK
	public static Integer qtdPessoasIdadeEntre18e25(List<Paciente> list) {
		final List<Paciente> listPacientes = list.stream()
				.filter(p -> p.getIdade() > 18 && p.getIdade() < 25)
				.collect(Collectors.toList());
		return listPacientes.size();
	}

	public static Paciente toBuild() {
		final Paciente paciente= Paciente.builder()
				//				.nome(faker.artist().name())
				.nome(faker.dragonBall().character())
				.idade(faker.number().numberBetween(10, 20))
				.altura(faker.number().randomDouble(2, 1, 2))
				.peso(71)
				.sexo(SexoEnum.valueByCodigo(faker.number().numberBetween(0, 1)))
				.build();
		return paciente;
	}

	public static void situacaoInteressante(List<Paciente> list) {
	}
}

