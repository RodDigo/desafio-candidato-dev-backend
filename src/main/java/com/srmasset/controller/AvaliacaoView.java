package com.srmasset.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.github.javafaker.Faker;

import srm.srmassset.pojos.Paciente;

public class AvaliacaoView extends AvaliacaoController {

	public static final Locale BRAZIL = new Locale("pt", "BR");
	public static final Faker faker = new Faker(BRAZIL);

	public static void main(String[] args) {

		final List<String> listString  = new ArrayList<>();

		final List<Paciente> list = criaListaPaciente();
		//		listString.add(" A média de idade entre Pacientes do sexo masculino é de "+ mediaIdadePacientesHomens(list).toString()+ " anos de idade"); // OK
		//		listString.add(" A Paciante " + nomeMulherMaisBaixa(list) + " é a de menor estatura."); // OK
		nomePacientesMaisVelhos(list);
		//		listString.add(qtdMulheresEntreAlturasAcimaPeso(list));
		//				mediaIdadePacientes(list);
		//		qtdPessoasIdadeEntre18e25(list);
		//		situacaoInteressante(list);

		listString.forEach(System.out::println);
	}

}

